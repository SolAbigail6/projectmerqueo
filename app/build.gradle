plugins {
    id 'com.android.application'
    id 'kotlin-android'
    id 'kotlin-kapt'
    id 'androidx.navigation.safeargs.kotlin'
}

android {
    compileSdkVersion 30
    buildToolsVersion "30.0.3"

    defaultConfig {
        applicationId "com.solabigail.projectbasemerqueo"
        minSdkVersion 16
        targetSdkVersion 30
        versionCode 1
        multiDexEnabled true
        versionName "1.0"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        def addConstant = { target, constantName, constantValue ->
            target.manifestPlaceholders += [(constantName): constantValue]
            target.buildConfigField "String", "${constantName}", "\"${constantValue}\""
        }
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
        debug {
            debuggable true
            addConstant(owner, "BASE_URL", "https://api.themoviedb.org/3/movie/")
        }
    }
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }
    viewBinding {
        enabled = true
    }
    kapt { generateStubs = true }
}

dependencies {

    implementation "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    implementation 'androidx.core:core-ktx:1.5.0'
    implementation 'androidx.appcompat:appcompat:1.3.0'
    implementation 'com.google.android.material:material:1.3.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.4'
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.2'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.3.0'

    //ktx
    implementation "androidx.core:core-ktx:$ktx_version"
    implementation "androidx.activity:activity-ktx:$ktx_activity_version"
    implementation "androidx.fragment:fragment-ktx:$ktx_version"

    //navigation
    implementation "androidx.navigation:navigation-fragment-ktx:$navigation_version"
    implementation "androidx.navigation:navigation-ui-ktx:$navigation_version"

    //retrofit
    implementation "com.squareup.retrofit2:retrofit:$retrofit_version"
    implementation "com.squareup.retrofit2:converter-moshi:$moshi_converter_version"
    implementation "com.squareup.retrofit2:converter-gson:$gson_converter_version"
    implementation 'com.squareup.okhttp3:logging-interceptor:4.8.1'

    //Moshi
    implementation "com.squareup.moshi:moshi-kotlin:$moshi_version"
    kapt "com.squareup.moshi:moshi-kotlin-codegen:$moshi_version"

    //rx-java
    implementation "io.reactivex.rxjava3:rxandroid:$rxjava3_version"
    implementation "io.reactivex.rxjava3:rxjava:$rxjava3_version"
    implementation "com.squareup.retrofit2:adapter-rxjava3:$retrofit_version"

    //room database
    implementation "androidx.room:room-runtime:$room_version"
    implementation "androidx.room:room-ktx:$room_version"
    kapt "androidx.room:room-compiler:$room_version"


    //extensions , viewmodel, livedata
    implementation "androidx.lifecycle:lifecycle-extensions:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"

    //Coroutines
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutines_version"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version"

    //dagger
    implementation "com.google.dagger:dagger:$dagger_version"
    //noinspection OutdatedLibrary
    implementation "com.google.dagger:dagger-android:$dagger_version"
    implementation "com.google.dagger:dagger-android-support:$dagger_version"
    kapt "com.google.dagger:dagger-compiler:$dagger_version"
    kapt "com.google.dagger:dagger-android-processor:$dagger_version"

    //multidex
    implementation "androidx.multidex:multidex:$multidex_version"

    //for recycler
    implementation "com.xwray:groupie:$groupie_version"
    implementation "com.xwray:groupie-viewbinding:$groupie_version"
    implementation "com.xwray:groupie-kotlin-android-extensions:$groupie_version"

    //glide
    annotationProcessor "com.github.bumptech.glide:compiler:$glide_version"
    implementation "com.github.bumptech.glide:glide:$glide_version"
}