package com.solabigail.projectbasemerqueo.features.movie.presentation

import com.solabigail.projectbasemerqueo.core.utils.ViewBase
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import javax.inject.Inject

class DetailMovieViewModel@Inject constructor(
    private val repository: MovieRepository
): ViewBase() {

    var movie: Flow<Movie> = emptyFlow()

    fun getMovieById(id: Int) {
        movie = repository.getMovieById(id)
    }

    fun addItemCard(id: Int) =
        repository.addCantItem(id)

    fun deleteCard(idMovie: Int, idCard: Int) = repository.deleteCard(idMovie, idCard)

    fun updateCard(idMovie: Int, idCard: Int, cant: Int) = repository.updateCard(idMovie, idCard, cant)

}