package com.solabigail.projectbasemerqueo.features.movie.presentation.callback

interface CallbackMovie {
    fun onClickMovie(idMovie: Int)
    fun onClickAddCardMovie(idMovie: Int, position: Int)
    fun onClickDeleteCard(idMovie: Int, idCard: Int, position: Int)
    fun onClickUpdateCard(idCard: Int, idMovie: Int, cat: Int, position: Int)
}