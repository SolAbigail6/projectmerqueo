package com.solabigail.projectbasemerqueo.features.movie.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.solabigail.projectbasemerqueo.core.utils.Constants
import com.solabigail.projectbasemerqueo.core.utils.convertToCurrency
import com.solabigail.projectbasemerqueo.databinding.ItemCardBinding
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.presentation.callback.CallbackMovie

class CardAdapter(val movies: MutableList<Movie>,
                  val callbackMovie: CallbackMovie
): RecyclerView.Adapter<CardAdapter.CardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val itemCardBinding = ItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CardViewHolder(itemCardBinding)
    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) = holder.bind(movies[position], callbackMovie, position)

    override fun getItemCount(): Int = movies.size

    fun updateItem(position: Int, movie: Movie){
        this.movies[position] = movie
        notifyItemChanged(position)
    }

    fun addAll(movies: List<Movie>){
        this.movies.clear()
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    class CardViewHolder(
        private val viewBinding: ItemCardBinding
    ): RecyclerView.ViewHolder(viewBinding.root) {
        fun bind(movie: Movie, callback: CallbackMovie, position: Int) {
            viewBinding.txtTitle.text = movie.title
            viewBinding.txtPrice.text = movie.price.toString()
            Glide.with(viewBinding.root).load(Constants.ServicesName.URL_PATH+movie.urlImage).into(viewBinding.imgPhoto)
            when(movie.state){
                true -> {
                    viewBinding.clAddMovie.visibility = View.VISIBLE
                    viewBinding.txtMovieCant.text = movie.card!!.cant.toString()
                    viewBinding.txtPrice.text = convertToCurrency(movie.price)
                    viewBinding.txtCant.text = movie.card!!.cant.toString()
                    viewBinding.txtPriceTotal.text = convertToCurrency(movie.price*movie.card!!.cant)
                    when(movie.card!!.cant>1){
                        true -> {
                            viewBinding.btnDeleteMovieCard.visibility = View.GONE
                            viewBinding.btnLessMovie.visibility = View.VISIBLE
                        }
                        false -> {
                            viewBinding.btnDeleteMovieCard.visibility = View.VISIBLE
                            viewBinding.btnLessMovie.visibility = View.GONE
                        }
                    }
                }
                false -> {
                    viewBinding.clAddMovie.visibility = View.GONE
                }
            }
            viewBinding.imgPhoto.setOnClickListener {
                callback.onClickMovie(movie.id)
            }
            viewBinding.btnDeleteMovieCard.setOnClickListener {
                callback.onClickDeleteCard(movie.id, movie.card!!.id, position)
            }
            viewBinding.btnLessMovie.setOnClickListener {
                callback.onClickUpdateCard(movie.card!!.id, movie.id, movie.card!!.cant-1, position)
            }
            viewBinding.btnAddMovie.setOnClickListener {
                callback.onClickUpdateCard(movie.card!!.id, movie.id, movie.card!!.cant+1, position)
            }
        }
    }
}