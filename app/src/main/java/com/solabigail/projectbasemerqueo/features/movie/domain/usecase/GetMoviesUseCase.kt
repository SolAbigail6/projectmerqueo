package com.solabigail.projectbasemerqueo.features.movie.domain.usecase

import com.solabigail.projectbasemerqueo.core.domain.usecase.SingleUseCase
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(
    private val repository: MovieRepository
): SingleUseCase<List<Movie>>() {

    private var page: Int = 1

    override fun buildUseCaseSingle(): Single<List<Movie>> {
        return repository.getAllMoviesRemote(page)
    }

    fun saveAllMovies(movies: List<Movie>) = repository.saveMovies(movies)

    fun getAllMoviesLocal() = repository.getAllMoviesLocal()

    fun addCardItem(id: Int) = repository.addCantItem(id)

    fun deleteCard(idMovie: Int, idCard: Int) = repository.deleteCard(idMovie, idCard)

    fun updateCard(idMovie: Int, idCard: Int, cant: Int) = repository.updateCard(idMovie, idCard, cant)

    fun cantMovies() = repository.cantMovies()

}