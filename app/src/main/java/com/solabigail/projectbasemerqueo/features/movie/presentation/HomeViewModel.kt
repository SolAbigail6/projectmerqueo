package com.solabigail.projectbasemerqueo.features.movie.presentation

import android.util.Log
import com.solabigail.projectbasemerqueo.core.utils.ViewBase
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.domain.usecase.GetMoviesUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getMoviesUseCase: GetMoviesUseCase
): ViewBase() {

    var movies: Flow<List<Movie>> = getMoviesUseCase.getAllMoviesLocal()

    init {
        if(getMoviesUseCase.cantMovies()==0){
            getAllMovies()
        }
    }

    private fun getAllMovies(){
        getMoviesUseCase.execute(
            onSuccess = {
                getMoviesUseCase.saveAllMovies(it)
            },
            onError = {
                Log.d("HomeFragmentErr", it.message.toString())
            }
        )
    }

    fun addItemCard(id: Int) =
        getMoviesUseCase.addCardItem(id)

    fun deleteCard(idMovie: Int, idCard: Int) = getMoviesUseCase.deleteCard(idMovie, idCard)

    fun updateCard(idMovie: Int, idCard: Int, cant: Int) = getMoviesUseCase.updateCard(idMovie, idCard, cant)

}