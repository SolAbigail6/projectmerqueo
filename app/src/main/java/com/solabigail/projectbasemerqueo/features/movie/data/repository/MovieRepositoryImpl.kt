package com.solabigail.projectbasemerqueo.features.movie.data.repository

import com.solabigail.projectbasemerqueo.features.movie.data.source.local.MovieDataSourceLocal
import com.solabigail.projectbasemerqueo.features.movie.data.source.remote.MovieDataSourceRemote
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val dataSourceLocal: MovieDataSourceLocal,
    private val dataSourceRemote: MovieDataSourceRemote
): MovieRepository {

    override fun getAllMoviesLocal(): Flow<List<Movie>> =
        dataSourceLocal.getMovies()

    override fun getAllMoviesRemote(page: Int): Single<List<Movie>> =
        dataSourceRemote.getAllMovies(page)

    override fun saveMovies(movies: List<Movie>) =
        dataSourceLocal.insertMovies(movies)

    override fun getMovieById(id: Int): Flow<Movie> = dataSourceLocal.getMovieById(id)

    override fun cantCard(): Flow<Int> = dataSourceLocal.cantCard()

    override fun addCantItem(id: Int) = dataSourceLocal.insertCard(id)

    override fun deleteCard(idMovie: Int, idCard: Int) = dataSourceLocal.deleteCard(idMovie, idCard)

    override fun updateCard(idMovie: Int, idCard: Int, cant: Int) = dataSourceLocal.updateCard(idMovie, idCard, cant)

    override fun cantMovies(): Int = dataSourceLocal.cantMovies()

    override fun getMoviesCard(): Flow<List<Movie>> = dataSourceLocal.getMoviesCard()

    override fun deleteAllCard(state: Boolean) = dataSourceLocal.deleteAllCard(state)

    override fun getTotalCard(): Flow<Int> = dataSourceLocal.getTotalCard()
}