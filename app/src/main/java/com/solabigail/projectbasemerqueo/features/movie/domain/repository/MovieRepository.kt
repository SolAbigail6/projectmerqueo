package com.solabigail.projectbasemerqueo.features.movie.domain.repository

import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.flow.Flow

interface MovieRepository{
    fun getAllMoviesLocal(): Flow<List<Movie>>
    fun getAllMoviesRemote(page: Int): Single<List<Movie>>
    fun saveMovies(movies: List<Movie>)
    fun getMovieById(id: Int): Flow<Movie>
    fun cantCard(): Flow<Int>
    fun addCantItem(id: Int)
    fun deleteCard(idMovie: Int, idCard: Int)
    fun updateCard(idMovie: Int, idCard: Int, cant: Int)
    fun cantMovies(): Int
    fun getMoviesCard(): Flow<List<Movie>>
    fun deleteAllCard(state: Boolean)
    fun getTotalCard(): Flow<Int>
}