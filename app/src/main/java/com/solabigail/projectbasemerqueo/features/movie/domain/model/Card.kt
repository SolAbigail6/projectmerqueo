package com.solabigail.projectbasemerqueo.features.movie.domain.model

data class Card(
    val id: Int,
    val cant: Int,
    val idMovie: Int
)