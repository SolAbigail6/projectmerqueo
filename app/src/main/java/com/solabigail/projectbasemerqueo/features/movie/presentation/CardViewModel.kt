package com.solabigail.projectbasemerqueo.features.movie.presentation

import com.solabigail.projectbasemerqueo.core.utils.ViewBase
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CardViewModel@Inject constructor(
    val repository: MovieRepository
): ViewBase()  {

    var movies: Flow<List<Movie>> = repository.getMoviesCard()
    var total: Flow<Int> = repository.getTotalCard()

    fun addItemCard(id: Int) =
        repository.addCantItem(id)

    fun deleteCard(idMovie: Int, idCard: Int) = repository.deleteCard(idMovie, idCard)

    fun updateCard(idMovie: Int, idCard: Int, cant: Int) = repository.updateCard(idMovie, idCard, cant)
}