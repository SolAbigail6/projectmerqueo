package com.solabigail.projectbasemerqueo.features.movie.presentation

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.solabigail.projectbasemerqueo.R
import com.solabigail.projectbasemerqueo.core.di.factory.ViewModelFactory
import com.solabigail.projectbasemerqueo.databinding.ActivityMainBinding
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Fragment
import com.solabigail.projectbasemerqueo.features.movie.presentation.callback.CallbackFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), CallbackFragment{

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val viewModel: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setUpObservers()
        binding.btnCard.setOnClickListener {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.cardFragment);
        }
        binding.btnClean.setOnClickListener {
            viewModel.deleteCard()
        }
    }


    private fun setUpObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.cant.collect {
                binding.txtCantCard.text = it.toString()
            }
        }
    }

    override fun onChageFragment(fragment: Fragment) {
       with(binding){
           when(fragment){
               Fragment.HOME -> {
                   btnCard.visibility = View.VISIBLE
                   btnClean.visibility = View.GONE
               }
               Fragment.DETAIL -> {
                   btnCard.visibility = View.VISIBLE
                   btnClean.visibility = View.GONE
               }
               Fragment.CARD -> {
                   btnCard.visibility = View.GONE
                   btnClean.visibility = View.VISIBLE
               }
           }
       }
    }
}