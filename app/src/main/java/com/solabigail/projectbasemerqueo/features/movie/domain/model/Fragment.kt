package com.solabigail.projectbasemerqueo.features.movie.domain.model

enum class Fragment{
    HOME, DETAIL, CARD
}