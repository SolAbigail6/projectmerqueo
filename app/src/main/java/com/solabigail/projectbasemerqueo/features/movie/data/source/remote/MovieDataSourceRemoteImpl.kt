package com.solabigail.projectbasemerqueo.features.movie.data.source.remote

import com.solabigail.projectbasemerqueo.core.data.source.remote.dto.toDomain
import com.solabigail.projectbasemerqueo.core.data.source.remote.service.MovieApi
import com.solabigail.projectbasemerqueo.core.utils.Constants
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class MovieDataSourceRemoteImpl @Inject constructor(
    private val movieApi: MovieApi
): MovieDataSourceRemote {

    override fun getAllMovies(page: Int): Single<List<Movie>> =
        movieApi.getAllMovies(Constants.ServicesName.KEY, page).map {
                movies -> movies.results.map { it.toDomain() }
        }

}