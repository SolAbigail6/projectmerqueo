package com.solabigail.projectbasemerqueo.features.movie.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.solabigail.projectbasemerqueo.core.utils.Constants
import com.solabigail.projectbasemerqueo.core.utils.convertToCurrency
import com.solabigail.projectbasemerqueo.databinding.ItemMovieBinding
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.presentation.callback.CallbackMovie

class MovieAdapter(
    val movies: MutableList<Movie>, val callbackMovie: CallbackMovie
): RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val itemMovieBinding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(itemMovieBinding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) = holder.bind(movies[position], callbackMovie, position)

    override fun getItemCount(): Int = movies.size

    fun updateItem(position: Int, movie: Movie){
            this.movies[position] = movie
            notifyItemChanged(position)
    }

    fun addAll(movies: List<Movie>){
        this.movies.clear()
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    class MovieViewHolder(
        private val viewBinding: ItemMovieBinding
    ): RecyclerView.ViewHolder(viewBinding.root) {
        fun bind(movie: Movie, callback: CallbackMovie, position: Int) {
            viewBinding.txtTitle.text = movie.title
            viewBinding.txtPrice.text = convertToCurrency(movie.price)
            Glide.with(viewBinding.root).load(Constants.ServicesName.URL_PATH+movie.urlImage).into(viewBinding.imgPhoto)
            when(movie.state){
                true -> {
                    viewBinding.clAddMovie.visibility = View.VISIBLE
                    viewBinding.btnAddCard.visibility = View.GONE
                    viewBinding.txtMovieCant.text = movie.card!!.cant.toString()
                    when(movie.card!!.cant>1){
                        true -> {
                            viewBinding.btnDeleteMovieCard.visibility = View.GONE
                            viewBinding.btnLessMovie.visibility = View.VISIBLE
                        }
                        false -> {
                            viewBinding.btnDeleteMovieCard.visibility = View.VISIBLE
                            viewBinding.btnLessMovie.visibility = View.GONE
                        }
                    }
                }
                false -> {
                    viewBinding.btnAddCard.visibility = View.VISIBLE
                    viewBinding.clAddMovie.visibility = View.GONE
                }
            }
            viewBinding.imgPhoto.setOnClickListener {
                callback.onClickMovie(movie.id)
            }
            viewBinding.btnAddCard.setOnClickListener {
                callback.onClickAddCardMovie(movie.id, position)
            }
            viewBinding.btnDeleteMovieCard.setOnClickListener {
                callback.onClickDeleteCard(movie.id, movie.card!!.id, position)
            }
            viewBinding.btnLessMovie.setOnClickListener {
                callback.onClickUpdateCard(movie.card!!.id, movie.id, movie.card!!.cant-1, position)
            }
            viewBinding.btnAddMovie.setOnClickListener {
                callback.onClickUpdateCard(movie.card!!.id, movie.id, movie.card!!.cant+1, position)
            }
        }
    }
}