package com.solabigail.projectbasemerqueo.features.movie.presentation

import android.util.Log
import com.solabigail.projectbasemerqueo.core.utils.ViewBase
import com.solabigail.projectbasemerqueo.features.movie.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repository: MovieRepository
): ViewBase() {
    var cant: Flow<Int> = repository.cantCard()

    fun deleteCard(){
        repository.deleteAllCard(false)
    }
}