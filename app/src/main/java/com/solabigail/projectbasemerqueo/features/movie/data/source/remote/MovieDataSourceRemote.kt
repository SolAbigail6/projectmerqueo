package com.solabigail.projectbasemerqueo.features.movie.data.source.remote

import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import io.reactivex.rxjava3.core.Single

interface MovieDataSourceRemote {
    fun getAllMovies(page: Int): Single<List<Movie>>
}