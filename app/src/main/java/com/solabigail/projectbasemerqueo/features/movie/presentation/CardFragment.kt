package com.solabigail.projectbasemerqueo.features.movie.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.solabigail.projectbasemerqueo.core.di.factory.ViewModelFactory
import com.solabigail.projectbasemerqueo.core.utils.BaseState
import com.solabigail.projectbasemerqueo.core.utils.convertToCurrency
import com.solabigail.projectbasemerqueo.databinding.FragmentCardBinding
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Fragment
import com.solabigail.projectbasemerqueo.features.movie.presentation.adapter.CardAdapter
import com.solabigail.projectbasemerqueo.features.movie.presentation.callback.CallbackFragment
import com.solabigail.projectbasemerqueo.features.movie.presentation.callback.CallbackMovie
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class CardFragment: DaggerFragment(), CallbackMovie {
    private val binding: FragmentCardBinding by lazy {
        FragmentCardBinding.inflate(layoutInflater)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val viewModel: CardViewModel by viewModels { viewModelFactory }

    private var position: Int? = null
    private val adapterCard = CardAdapter(mutableListOf(), this)
    private lateinit var callbackFragment: CallbackFragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
        setUpAdapter()
        callbackFragment.onChageFragment(Fragment.CARD)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callbackFragment = activity as CallbackFragment
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement callbackFragment")
        }
    }

    private fun setUpObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.movies.collect {
                with(binding){
                    when(it.isEmpty()){
                        true -> {
                            txtEmpty.visibility = View.VISIBLE
                            adapterCard.addAll(emptyList())
                            clTotal.visibility = View.GONE
                        }
                        false -> {
                            txtEmpty.visibility = View.GONE
                            clTotal.visibility = View.VISIBLE
                            when(position!=null){
                                true -> adapterCard.updateItem(position!!, it[position!!])
                                false -> adapterCard.addAll(it)
                            }
                        }
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.total.collect {
                binding.txtTotalCard.text = convertToCurrency(it.toDouble())
            }
        }
    }

    private fun handleState(it: BaseState) {
        when (it) {
            BaseState.Loading -> showProgress()
            BaseState.HideLoading -> hideProgress()
        }
    }

    private fun setUpAdapter() {
        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvMovies.apply {
            adapter = adapterCard
            setLayoutManager(layoutManager)
        }
    }

    private fun showProgress() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        binding.progressBar.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        this.position = null
    }

    override fun onClickMovie(id: Int) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(id)
        findNavController().navigate(action)
    }

    override fun onClickAddCardMovie(id: Int, position: Int) {
        this.position = position
        viewModel.addItemCard(id)
    }

    override fun onClickDeleteCard(idMovie: Int, idCard: Int, position: Int) {
        this.position = position
        viewModel.deleteCard(idMovie, idCard)
    }

    override fun onClickUpdateCard(idCard: Int, idMovie: Int, cant: Int, position: Int) {
        this.position = position
        viewModel.updateCard(idMovie, idCard, cant)
    }
}