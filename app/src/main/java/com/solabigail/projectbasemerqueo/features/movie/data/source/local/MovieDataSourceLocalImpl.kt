package com.solabigail.projectbasemerqueo.features.movie.data.source.local

import com.solabigail.projectbasemerqueo.core.data.source.local.dao.CardDao
import com.solabigail.projectbasemerqueo.core.data.source.local.dao.MovieDao
import com.solabigail.projectbasemerqueo.core.data.source.local.vo.CardVo
import com.solabigail.projectbasemerqueo.core.data.source.local.vo.toDomain
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.solabigail.projectbasemerqueo.features.movie.domain.model.toVo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MovieDataSourceLocalImpl @Inject constructor(
    private val movieDao: MovieDao,
    private val cardDao: CardDao
): MovieDataSourceLocal {
    override fun getMovies(): Flow<List<Movie>> =
        movieDao.getAllMovies().map {
            it.map {
                val movie: Movie = it.toDomain()
                when(it.state){
                    true -> movie.card = cardDao.findCardVoByMovieVo(it.id).toDomain()
                }
                movie
            }
        }

    override fun insertMovies(movies: List<Movie>) =
        movieDao.insertMovies(movies.map { it.toVo() })

    override fun getMovieById(id: Int): Flow<Movie> =
        movieDao.getMovieById(id).map {
            val movie: Movie = it.toDomain()
            when(it.state){
                true -> movie.card = cardDao.findCardVoByMovieVo(it.id).toDomain()
            }
            movie
        }

    override fun cantCard(): Flow<Int> =
        cardDao.findCountCardVo()

    override fun insertCard(id: Int) {
        movieDao.updateMovieById(id, true)
        cardDao.insertCard(CardVo(0, id, 1))
    }

    override fun deleteCard(idMovie: Int, idCard: Int) {
        movieDao.updateMovieById(idMovie, false)
        cardDao.deleteCardVoById(idCard)
    }

    override fun updateCard(idMovie: Int, idCard: Int, cant:Int) {
        movieDao.updateMovieById(idMovie, true)
        cardDao.updateCardCardVoById(idCard, cant)
    }

    override fun cantMovies(): Int = movieDao.countMovies()

    override fun getMoviesCard(): Flow<List<Movie>> =
        movieDao.getMoviesCard().map {
            it.map {
                val movie: Movie = it.toDomain()
                when(it.state){
                    true -> movie.card = cardDao.findCardVoByMovieVo(it.id).toDomain()
                }
                movie
            }
        }

    override fun deleteAllCard(state: Boolean) {
        cardDao.deleteAllCardVo()
        movieDao.updateMovieAll(state)
    }

    override fun getTotalCard(): Flow<Int> = cardDao.getTotalCardVo()
}