package com.solabigail.projectbasemerqueo.features.movie.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.solabigail.projectbasemerqueo.core.di.factory.ViewModelFactory
import com.solabigail.projectbasemerqueo.core.utils.Constants
import com.solabigail.projectbasemerqueo.core.utils.convertToCurrency
import com.solabigail.projectbasemerqueo.databinding.FragmentDetailMovieBinding
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Fragment
import com.solabigail.projectbasemerqueo.features.movie.presentation.callback.CallbackFragment
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DetailMovieFragment: DaggerFragment()   {

    private val binding: FragmentDetailMovieBinding by lazy {
        FragmentDetailMovieBinding.inflate(layoutInflater)
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val viewModel: DetailMovieViewModel by viewModels { viewModelFactory }

    private val args: DetailMovieFragmentArgs by navArgs()
    private lateinit var callbackFragment: CallbackFragment

    private var idMovie: Int = 0
    private var idCard: Int = 0
    private var cant: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
        setUpData()
        callbackFragment.onChageFragment(Fragment.DETAIL)

        with(binding){
            btnAddCard.setOnClickListener {
                viewModel.addItemCard(idMovie)
            }
            btnDeleteMovieCard.setOnClickListener {
                viewModel.deleteCard(idMovie, idCard)
            }
            btnLessMovie.setOnClickListener {
                viewModel.updateCard(idMovie, idCard, cant-1)
            }
            btnAddMovie.setOnClickListener {
                viewModel.updateCard(idMovie, idCard, cant+1)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            callbackFragment = activity as CallbackFragment
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement callbackFragment")
        }
    }

    private fun setUpData(){
        viewModel.getMovieById(args.movieId)
    }

    private fun setUpObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.movie.collect {
                idMovie = it.id
               with(binding){
                   txtTitle.text = it.title
                   txtDescriptionMovie.text = it.description
                   txtPrice.text = convertToCurrency(it.price)
                   when(it.state){
                       true -> {
                           idCard = it.card!!.id
                           cant = it.card!!.cant
                           clAddMovie.visibility = View.VISIBLE
                           btnAddCard.visibility = View.GONE
                           txtMovieCant.text = it.card!!.cant.toString()
                           when(it.card!!.cant>1){
                               true -> {
                                   btnDeleteMovieCard.visibility = View.GONE
                                   btnLessMovie.visibility = View.VISIBLE
                               }
                               false -> {
                                   btnDeleteMovieCard.visibility = View.VISIBLE
                                   btnLessMovie.visibility = View.GONE
                               }
                           }
                       }
                       false -> {
                           btnAddCard.visibility = View.VISIBLE
                           clAddMovie.visibility = View.GONE
                       }
                   }
                   Glide.with(root).load(Constants.ServicesName.URL_PATH+it.urlImage).into(imgPhoto)
               }
            }
        }
    }
}