package com.solabigail.projectbasemerqueo.features.movie.domain.model

import com.solabigail.projectbasemerqueo.core.data.source.local.vo.MovieVo

data class Movie(
    val id: Int,
    val title: String,
    val urlImage: String,
    val price: Double,
    val date: String,
    val average: Double,
    val state: Boolean,
    val description: String,
    var card: Card? = null
)

fun Movie.toVo() = MovieVo(
    id = id,
    title = title,
    urlImage = urlImage,
    price = price,
    date = date,
    average = average,
    description = description,
    state =  state
)