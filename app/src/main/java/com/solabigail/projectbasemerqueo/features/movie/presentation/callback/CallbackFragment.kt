package com.solabigail.projectbasemerqueo.features.movie.presentation.callback

import com.solabigail.projectbasemerqueo.features.movie.domain.model.Fragment

interface CallbackFragment {
    fun onChageFragment(fragment: Fragment)
}