package com.solabigail.projectbasemerqueo.features.movie.data.source.local

import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import kotlinx.coroutines.flow.Flow

interface MovieDataSourceLocal {
    fun getMovies(): Flow<List<Movie>>
    fun insertMovies(movies: List<Movie>)
    fun getMovieById(id: Int): Flow<Movie>
    fun cantCard(): Flow<Int>
    fun insertCard(id: Int)
    fun deleteCard(idMovie: Int, idCard: Int)
    fun updateCard(idMovie: Int, idCard: Int, cant: Int)
    fun cantMovies(): Int
    fun getMoviesCard(): Flow<List<Movie>>
    fun deleteAllCard(state: Boolean)
    fun getTotalCard(): Flow<Int>
}