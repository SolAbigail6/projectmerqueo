package com.solabigail.projectbasemerqueo.core.di.module.db

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.solabigail.projectbasemerqueo.core.data.source.local.AppDatabase
import com.solabigail.projectbasemerqueo.core.data.source.local.dao.CardDao
import com.solabigail.projectbasemerqueo.core.data.source.local.dao.MovieDao
import com.solabigail.projectbasemerqueo.core.utils.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, Constants.NAME_DB)
            .allowMainThreadQueries()
            .build()
    }

    @Provides
    fun provideMovieDao(RoomDatabase: AppDatabase): MovieDao {
        return RoomDatabase.movieDao()
    }

    @Provides
    fun provideCardDao(RoomDatabase: AppDatabase): CardDao {
        return RoomDatabase.cardDao()
    }
}