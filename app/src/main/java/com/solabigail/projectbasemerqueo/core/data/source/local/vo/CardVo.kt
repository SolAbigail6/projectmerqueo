package com.solabigail.projectbasemerqueo.core.data.source.local.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Card

@Entity
class CardVo(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val idMovie: Int,
    val cant: Int
)

fun CardVo.toDomain() = Card(
    id = id,
    idMovie = idMovie,
    cant = cant
)