package com.solabigail.projectbasemerqueo.core.data.source.local.dao

import androidx.room.*
import com.solabigail.projectbasemerqueo.core.data.source.local.vo.CardVo
import kotlinx.coroutines.flow.Flow


@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCard(card: CardVo)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateCard(card: CardVo)

    @Query("SELECT * FROM CardVo LIMIT 1")
    fun findCardVo(): Flow<CardVo>

    @Query("SELECT * FROM CardVo WHERE idMovie = :idMovie")
    fun findCardVoByMovieVo(idMovie: Int): CardVo

    @Query("DELETE FROM CardVo WHERE id = :idCard")
    fun deleteCardVoById(idCard: Int)

    @Query("DELETE FROM CardVo")
    fun deleteAllCardVo()

    @Query("UPDATE CardVo SET cant=:cantCard WHERE id = :idCard")
    fun updateCardCardVoById(idCard: Int, cantCard: Int)

    @Delete
    fun deleteCardVo(card: CardVo)

    @Query("SELECT CASE WHEN SUM(cant) IS NULL THEN 0 ELSE SUM(cant) END  FROM CardVo")
    fun findCountCardVo(): Flow<Int>

    @Query("SELECT " +
            "CASE WHEN SUM(MovieVo.price * CardVo.cant) IS NULL THEN 0 ELSE SUM(MovieVo.price * CardVo.cant) END " +
            "FROM MovieVo INNER JOIN CardVo ON MovieVo.id = CardVo.idMovie " +
            "WHERE state = 1")
    fun getTotalCardVo(): Flow<Int>
}