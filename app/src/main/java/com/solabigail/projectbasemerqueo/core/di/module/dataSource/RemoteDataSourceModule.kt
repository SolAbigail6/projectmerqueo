package com.solabigail.projectbasemerqueo.core.di.module.dataSource

import com.solabigail.projectbasemerqueo.features.movie.data.source.remote.MovieDataSourceRemote
import com.solabigail.projectbasemerqueo.features.movie.data.source.remote.MovieDataSourceRemoteImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RemoteDataSourceModule {
    @Binds
    abstract fun bindMovieRemoteDataSource(dataSource: MovieDataSourceRemoteImpl): MovieDataSourceRemote
}