package com.solabigail.projectbasemerqueo.core.utils

sealed class BaseState {

    object Loading : BaseState()
    object HideLoading : BaseState()

    class ShowMessage(
        val message: String
    ) : BaseState()
}