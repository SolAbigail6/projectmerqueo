package com.solabigail.projectbasemerqueo.core.utils

import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.Flow
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

open class ViewBase : ViewModel() {

    val _state = MutableStateFlow<BaseState>(BaseState.Loading)
    val state: StateFlow<BaseState>
        get() = _state

    fun loading() {
        _state.value = BaseState.Loading
    }

    fun offLoading() {
        _state.value = BaseState.HideLoading
    }
}