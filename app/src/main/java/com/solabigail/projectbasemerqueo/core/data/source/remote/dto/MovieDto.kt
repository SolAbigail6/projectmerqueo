package com.solabigail.projectbasemerqueo.core.data.source.remote.dto

import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie
import com.squareup.moshi.Json

data class MovieDto (
    @Json(name = "id") val id: Int,
    @Json(name = "title") val title: String,
    @Json(name = "backdrop_path") val urlImage: String,
    @Json(name = "popularity") val price: Double,
    @Json(name = "release_date") val date: String,
    @Json(name = "vote_average") val average: Double,
    @Json(name = "overview") val description: String
    )

fun MovieDto.toDomain() = Movie(
    id = id,
    title = title,
    urlImage = urlImage,
    price = price,
    date = date,
    average = average,
    description = description,
    state = false
)