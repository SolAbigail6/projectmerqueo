package com.solabigail.projectbasemerqueo.core.data.source.remote.service

import com.solabigail.projectbasemerqueo.core.data.source.remote.dto.MovieDto
import com.solabigail.projectbasemerqueo.core.data.source.remote.dto.ResponseBase
import com.solabigail.projectbasemerqueo.core.utils.Constants
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {
    @GET(Constants.ServicesName.TOP_RATED)
    fun getAllMovies(@Query("api_key") key: String, @Query("page") page: Int): Single<ResponseBase>
}