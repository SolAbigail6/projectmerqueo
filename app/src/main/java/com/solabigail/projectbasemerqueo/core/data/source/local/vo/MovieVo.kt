package com.solabigail.projectbasemerqueo.core.data.source.local.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.solabigail.projectbasemerqueo.features.movie.domain.model.Movie

@Entity
data class MovieVo (
    @PrimaryKey(autoGenerate = false) val id: Int,
    val title: String,
    val urlImage: String,
    val price: Double,
    val date: String,
    val average: Double,
    val description: String,
    val state: Boolean
)

fun MovieVo.toDomain() = Movie(
    id = id,
    title = title,
    urlImage = urlImage,
    price = price,
    date = date,
    average = average,
    state = state,
    description = description
)