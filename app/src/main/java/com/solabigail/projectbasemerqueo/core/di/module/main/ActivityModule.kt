package com.solabigail.projectbasemerqueo.core.di.module.main

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.solabigail.projectbasemerqueo.features.movie.presentation.MainActivity

@Module(
    includes = [FragmentModule::class]
)
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}