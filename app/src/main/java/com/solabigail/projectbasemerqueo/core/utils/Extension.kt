package com.solabigail.projectbasemerqueo.core.utils

import java.text.NumberFormat
import java.util.*

fun convertToCurrency(valor: Double): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.CANADA)
    val currency: String = format.format(valor)
    return currency
}