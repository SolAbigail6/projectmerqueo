package com.solabigail.projectbasemerqueo.core.di.module.vm

import androidx.lifecycle.ViewModel
import com.solabigail.projectbasemerqueo.core.di.annotation.ViewModelKey
import com.solabigail.projectbasemerqueo.features.movie.presentation.CardViewModel
import com.solabigail.projectbasemerqueo.features.movie.presentation.DetailMovieViewModel
import com.solabigail.projectbasemerqueo.features.movie.presentation.HomeViewModel
import com.solabigail.projectbasemerqueo.features.movie.presentation.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailMovieViewModel::class)
    abstract fun bindDetailMovieViewModel(movieViewModel: DetailMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CardViewModel::class)
    abstract fun bindCardViewModel(cardViewModel: CardViewModel): ViewModel

}