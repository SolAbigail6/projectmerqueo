package com.solabigail.projectbasemerqueo.core.di.module.dataSource

import com.solabigail.projectbasemerqueo.features.movie.data.source.local.MovieDataSourceLocal
import com.solabigail.projectbasemerqueo.features.movie.data.source.local.MovieDataSourceLocalImpl
import dagger.Binds
import dagger.Module

@Module
abstract class LocalDataSourceModule {

    @Binds
    abstract fun bindMovieLocalDataSource(dataSource: MovieDataSourceLocalImpl): MovieDataSourceLocal

}