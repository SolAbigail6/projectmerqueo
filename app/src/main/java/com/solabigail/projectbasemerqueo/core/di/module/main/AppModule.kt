package com.solabigail.projectbasemerqueo.core.di.module.main

import android.content.Context
import android.content.res.Resources
import com.solabigail.projectbasemerqueo.core.AppBase
import com.solabigail.projectbasemerqueo.core.di.module.vm.ViewModelModule
import com.solabigail.projectbasemerqueo.core.utils.SharedPreferencesManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, ActivityModule::class])
class AppModule {
    @Singleton
    @Provides
    fun provideContext(application: AppBase): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideResources(application: AppBase): Resources = application.resources

    //preferences
    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferencesManager {
        return SharedPreferencesManager(context)
    }
}