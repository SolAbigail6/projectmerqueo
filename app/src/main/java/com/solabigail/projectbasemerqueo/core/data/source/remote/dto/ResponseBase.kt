package com.solabigail.projectbasemerqueo.core.data.source.remote.dto

import com.squareup.moshi.Json

data class ResponseBase(
    @Json(name = "page") val page: Int,
    @Json(name = "results") val results: List<MovieDto>
)