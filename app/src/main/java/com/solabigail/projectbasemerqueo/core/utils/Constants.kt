package com.solabigail.projectbasemerqueo.core.utils

object Constants {

    const val NAME_DB = "merqueo"

    object SharedPreferences {
        const val PREF_NAME = "app_preferences"
        const val PREF_LANGUAGE_SELECTED = "language_selected"
        const val PREF_KEY_ACCESS = "key_access"
    }

    object ServicesName {
        const val TOP_RATED = "top_rated"
        const val URL_PATH = "https://image.tmdb.org/t/p/original"
        const val KEY = "b881b6df72c0edc82144317f87f5da5e"
    }
}