package com.solabigail.projectbasemerqueo.core.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.solabigail.projectbasemerqueo.core.data.source.local.dao.CardDao
import com.solabigail.projectbasemerqueo.core.data.source.local.dao.MovieDao
import com.solabigail.projectbasemerqueo.core.data.source.local.vo.CardVo
import com.solabigail.projectbasemerqueo.core.data.source.local.vo.MovieVo

@Database(entities = [MovieVo::class, CardVo::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun cardDao(): CardDao
}