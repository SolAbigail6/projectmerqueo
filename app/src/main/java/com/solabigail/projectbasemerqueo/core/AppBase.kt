package com.solabigail.projectbasemerqueo.core

import androidx.multidex.MultiDex
import com.solabigail.projectbasemerqueo.core.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class AppBase: DaggerApplication(){

    private val applicationInjector = DaggerAppComponent.builder().application(this).build()

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return applicationInjector
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(applicationContext)
    }

}