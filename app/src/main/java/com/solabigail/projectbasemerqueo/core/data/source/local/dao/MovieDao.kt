package com.solabigail.projectbasemerqueo.core.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.solabigail.projectbasemerqueo.core.data.source.local.vo.MovieVo
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(movie: MovieVo)

    @Query("SELECT * FROM MovieVo")
    fun getAllMovies(): Flow<List<MovieVo>>

    @Query("SELECT COUNT(*) FROM MovieVo")
    fun countMovies(): Int

    @Query("SELECT * FROM MovieVo WHERE id=:id")
    fun getMovieById(id: Int): Flow<MovieVo>

    @Query("SELECT * FROM MovieVo WHERE state=1")
    fun getMoviesCard(): Flow<List<MovieVo>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovies(movies: List<MovieVo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movies: MovieVo)

    @Query("UPDATE MovieVo SET state=:state WHERE id=:id")
    fun updateMovieById(id: Int, state: Boolean)

    @Query("UPDATE MovieVo SET state=:state")
    fun updateMovieAll(state: Boolean)
}