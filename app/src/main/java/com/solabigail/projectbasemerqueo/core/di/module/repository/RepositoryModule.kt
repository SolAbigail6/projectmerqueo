package com.solabigail.projectbasemerqueo.core.di.module.repository

import com.solabigail.projectbasemerqueo.features.movie.data.repository.MovieRepositoryImpl
import com.solabigail.projectbasemerqueo.features.movie.domain.repository.MovieRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
    @Binds
    abstract fun bindMovieRepository(repository: MovieRepositoryImpl): MovieRepository

}