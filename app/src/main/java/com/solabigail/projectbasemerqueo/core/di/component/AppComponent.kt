package com.solabigail.projectbasemerqueo.core.di.component


import com.solabigail.projectbasemerqueo.core.AppBase
import com.solabigail.projectbasemerqueo.core.di.module.db.DatabaseModule
import com.solabigail.projectbasemerqueo.core.di.module.dataSource.LocalDataSourceModule
import com.solabigail.projectbasemerqueo.core.di.module.dataSource.RemoteDataSourceModule
import com.solabigail.projectbasemerqueo.core.di.module.main.AppModule
import com.solabigail.projectbasemerqueo.core.di.module.network.ApiModule
import com.solabigail.projectbasemerqueo.core.di.module.network.NetworkModule
import com.solabigail.projectbasemerqueo.core.di.module.repository.RepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, DatabaseModule::class, ApiModule::class,
    RepositoryModule::class, LocalDataSourceModule::class, RemoteDataSourceModule::class])
interface AppComponent: AndroidInjector<AppBase> {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: AppBase): Builder

        fun build(): AppComponent
    }

    override fun inject(app: AppBase)
}