package com.solabigail.projectbasemerqueo.core.di.module.main

import com.solabigail.projectbasemerqueo.features.movie.presentation.CardFragment
import com.solabigail.projectbasemerqueo.features.movie.presentation.DetailMovieFragment
import com.solabigail.projectbasemerqueo.features.movie.presentation.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDetailMovieFragment(): DetailMovieFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCardFragment(): CardFragment
}